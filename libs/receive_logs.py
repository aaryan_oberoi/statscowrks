import pika


# Set the connection parameters to connect to rabbit-server1 on port 5672
# on the / virtual host using the username "guest" and password "guest"
credentials = pika.PlainCredentials('rabbitmq', 'rabbitmq')
parameters = pika.ConnectionParameters('127.0.0.1',
                                       5672,
                                       '/',
                                       credentials)
# credentials = pika.PlainCredentials('rabbitmq', 'rabbitmq')
connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.exchange_declare(exchange='ex3', exchange_type='fanout')
channel.exchange_declare(exchange='ex5', exchange_type='fanout')
channel.exchange_declare(exchange='ex778', exchange_type='fanout')

channel.queue_declare(queue='q3_1526361846.2431898', exclusive=True)
channel.queue_declare(queue='q3_1526361875.2454545', exclusive=True)
channel.queue_declare(queue='q5_1526361845.2431898', exclusive=True)
channel.queue_declare(queue='q778_1526361846.4565456', exclusive=True)
# channel.queue_declare(queue='Device5', exclusive=True)
# channel.queue_declare(queue='Device6', exclusive=True)
# channel.queue_declare(queue='Device7', exclusive=True)
# channel.queue_declare(queue='Device8', exclusive=True)
# channel.queue_declare(queue='Device9', exclusive=True)

channel.queue_bind(exchange='ex3', queue='q3_1526361846.2431898')
channel.queue_bind(exchange='ex5', queue='q3_1526361875.2454545')
channel.queue_bind(exchange='ex778', queue='q5_1526361845.2431898')
channel.queue_bind(exchange='ex778', queue='q778_1526361846.4565456')
# channel.queue_bind(exchange='User4', queue='Device5')
# channel.queue_bind(exchange='User5', queue='Device6')
# channel.queue_bind(exchange='User5', queue='Device7')
# channel.queue_bind(exchange='User6', queue='Device8')
# channel.queue_bind(exchange='User7', queue='Device8')
# channel.queue_bind(exchange='User7', queue='Device9')
# channel.queue_bind(exchange='User7', queue='Device9')

print(' [*] Waiting for logs. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print(" [x] %r" % body)


channel.basic_consume(callback, queue='q3_1526361846.2431898', no_ack=True)
# channel.basic_consume(callback, queue='Device2', no_ack=True)
# channel.basic_consume(callback, queue='Device3', no_ack=True)
channel.start_consuming()
