import React, { Component } from 'react';
import Card from 'material-ui/Card';
import '../App.css';
import dataFetch from './apicall';

var refresh_interval = 5000; // 5 sec

class PaperSheet extends Component {
	constructor(props){
        super(props);
        this.state={
            userCount: 0,
            deviceCount: 0,
            epochTime: 0
        };
        this.callBack = this.callBack.bind(this)
        this.callapi = this.callapi.bind(this)
        this.callapi();
    }
    // const mappingFunction = p => p.usercount;
    // const names = data.map(mappingFunction);
    
    callBack(data){
        // console.log(data)
        // let TempUser = []
        // data.map((item, index) => {
        //     TempUser.push(item.usercount)
        // })
        console.log(data);
        const userList = data.map(p => p.usercount);
        const devList = data.map(q => q.devicecount);
        const epochList = data.map(r => r.epoch);
        //console.log(userList)
        this.setState({
            deviceCount: devList[0],
            userCount: userList[0],
            epochTime: epochList[0]
        })
        setTimeout(this.callapi, refresh_interval)
    }
    callapi(){
        dataFetch(this.callBack);
    }
    render() {
        return (
            <div >
                    <Card className="paper1 col-xs-6 col-sm-3" elevation={4}>
                        <h3>
                        <div>{this.state.deviceCount}</div>
                        </h3>
                        <p>
                        Devices Online
                        </p>
                    </Card>
                    <Card className="paper2  col-xs-6 col-sm-3" elevation={4} >
                        <h3>
                        <div>{this.state.userCount}</div>
                        </h3>
                        <p>
                        Users Online
                        </p>
                    </Card>
                    <div className="clearfix" />
            </div>
        );
    }
}


export default PaperSheet;
