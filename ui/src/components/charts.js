import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Bar} from 'react-chartjs-2';
import dataFetch from './apicall';
import {Line} from 'react-chartjs-2';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import '../App.css';
var refresh_interval = 60000; // 1 min
export default class BarChart extends Component{

  constructor(props){
  super(props);
	this.state={
		userCount: 0,
		deviceCount: 0,
		epochTime: 0,
        dateCount: 0 
	};
    this.callBack = this.callBack.bind(this)
    this.callapi = this.callapi.bind(this)
    this.callapi();
    }
    callBack(data){
      // console.log(data)
      const userList = data.map(p => p.usercount);
      const devList = data.map(q => q.devicecount);
      const epochList = data.map(r => r.epoch);
      const dateList = data.map(s => s.date);
	  console.log(devList)
	  console.log(userList)
      this.setState({
          deviceCount: devList,
          userCount: userList,
          epochTime: epochList,
          dateCount: dateList
	  })
	//   let TempUser = []
    //   data.map((item, index) => {
    //       TempUser.push(item.userList)
    //   })
      setTimeout(this.callapi, refresh_interval)
  }
  callapi(){
      dataFetch(this.callBack);
  }

  render() {
    return (
      <div className="chart">
            <Bar 
              data={{
                  datasets: [{
                  label: 'User Count',
                  type:'line',
                  data: this.state.userCount,
                  fill: false,
                  borderColor: '#EC932F',
                  backgroundColor: '#EC932F',
                  pointBorderColor: '#000000',
                  pointBackgroundColor: '#EC932F',
                  pointHoverBackgroundColor: '#EC932F',
                  pointHoverBorderColor: '#EC932F',
                  yAxisID: 'y-axis-2'
                  },{
                  type: 'line',
                  label: 'Device Count',
                  data: this.state.deviceCount,
                  fill: false,
                  backgroundColor: '#71B37C',
                  borderColor: '#71B37C',
                  pointBorderColor: '#000000',
                  hoverBackgroundColor: '#71B37C',
                  hoverBorderColor: '#71B37C',
                  yAxisID: 'y-axis-1'
              }]}}
              options={{responsive: true,
                  labels: this.state.dateCount,
                  tooltips: {
                  mode: 'label'
                  },
                  elements: {
                  line: {
                  fill: false
                  }
                  },
                  scales: {

                  xAxes: [
                    {
                      display: true,
                      gridLines: {
                        display: true
                      },

                      labels: this.state.dateCount,
                    }
                  ],
                  yAxes: [
                    {
                      type: 'linear',
                      display: true,
                      position: 'left',
                      id: 'y-axis-1',
                      gridLines: {
                        display: true
                      },
                      labels: {
                        show: true
                      }
                    },
                    {
                      type: 'linear',
                      display: true,
                      position: 'right',
                      id: 'y-axis-2',
                      gridLines: {
                        display: true
                      },
                      labels: {
                        show: true
                      }
                    }
                  ]
              }}}

          />

        </div>
      );
    }
  };
