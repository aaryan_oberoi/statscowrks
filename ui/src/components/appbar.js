import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import '../App.css';


class ButtonAppBar extends Component {
    render() {
        return (
            <div className="appbar">    
            <AppBar position="static">
                <Toolbar>
                    <h2>
                    User Statistics
                    </h2>
                </Toolbar>
            </AppBar>
            </div>
        );
    }
}

export default ButtonAppBar;
