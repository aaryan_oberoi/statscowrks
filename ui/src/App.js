import React, { Component } from 'react';
import './App.css';
import ButtonAppBar from './components/appbar';
import PaperSheet from './components/paper';
import BarChart from './components/charts'


class App extends Component {
    render() {
        return (
            <div>
	            <ButtonAppBar/>
	            <PaperSheet/>
	            <BarChart/>
            </div>
        );
    }
}

export default App;
