import tornado.ioloop
import tornado.web
import json
from pymongo import MongoClient
from libs.users import getUsers
client = MongoClient()
db = client.test_database


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        try:
            activity = getUsers()
            resp = json.dumps(activity, default=str)
            # resp.body = json.dumps(present_data) -- to send only current data
            self.write(resp)
            self.set_header('Content-Type', 'application/json')
            print('Response >>>>>>>>>>>>', resp)
        except Exception as e:
            raise e


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    app.listen(8000)
    tornado.ioloop.IOLoop.current().start()
